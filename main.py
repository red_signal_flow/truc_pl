# -*- coding: iso-8859-1 -*-

from pygame import event, mouse,init,display
from pygame.time import Clock
from pygame.locals import *
from sys import exit
from truc.deck import Deck, Card
from truc.player import Player
from truc.truc import Truc

class Game(object):
    def __init__(self,x,y):
        init()
        self.clock = Clock()
        flag = DOUBLEBUF
        self.screen_size = (x,y)
        self.surface = display.set_mode(self.screen_size,flag)
        self.running = True
        self.loop()

    def game_exit(self):
        exit()
    def update_surface(self):
        display.flip()
    def clear(self):
        self.surface.fill((0,255,0))

    def handle_event(self, truc):
        for e in event.get():
            if e.type==QUIT:
                self.running = False
            if e.type==MOUSEBUTTONDOWN:
                truc.waiting_next_player_play()

    def loop(self):
        truc = Truc(self.surface)
        players_name = ["macartur","joao","lucio","ana"]

        truc.add_players(players_name)
        truc.players_get_cards()

        while (truc.winner() and self.running):
            self.clock.tick(60)
            truc.draw()
            self.update_surface()
            self.handle_event(truc)
            self.clear()

        self.game_exit()
if __name__ == "__main__":
    Game(800,600)
