from deck import card_size
from pygame import mouse

player_position = ["left","up","right","down"]
board = 30


class PlayerPosition(object):
    def __init__(self,position,surface):
        self.m_sw,self.m_sh = surface.get_size() 
        self.position_name = position

        if position == player_position[0]:
            x = 0
            y = self.m_sh - card_size[1] - board

        if position == player_position[1]:
            x = self.m_sw/2 - card_size[0] - board
            y = 0

        if position == player_position[2]:
            x= self.m_sw - card_size[0]  
            y = self.m_sh - card_size[1] - board

        if  position == player_position[3]:
            x = self.m_sw/2 - card_size[0] - board
            y = self.m_sh - card_size[1]

        self.position = (x,y)
        self.__next = 0

    def next_element(self):
        if self.__next == 0: 
            self.__next += 1
            return self.position
        x,y = self.position
        if self.position_name in player_position[0]:
            y = self.__next*(card_size[1]+board)
        if self.position_name in player_position[1]:
            x += self.__next*(card_size[0]+board)
        if self.position_name in player_position[2]:
            y = self.__next*(card_size[1]+board)
        if self.position_name in player_position[3]:
            x += self.__next*(card_size[0]+board)

        self.__next+=1

        if self.__next > 2:  self.__next = 0
        return (x,y)


class Player(object):
    def __init__(self,name,surface,position):
        self.name = name
        self.surface = surface
        self.hand = list()
        self.position = PlayerPosition(position,self.surface) 

    def name(self):
        return self.name

    def draw(self):
        for card in self.hand:
            card.draw_card(self.surface)

    def pick_card(self,card):
        x,y = self.position.next_element()
        card.set_position(x,y)
        self.hand.append(card)

    def play_card(self):
       x,y = mouse.get_pos()
       for card in range(len(self.hand)):
        if self.hand[card].hover(x,y): 
            current_card = self.hand.pop(card)
            return  current_card
       return False
