# -*- coding: iso-8859-1 -*-

import pygame
from pygame.locals import *
from random import shuffle

naipes = {"copas":0,"ouros":1, "paus":2, "espada":3}
values = {"2":0, "3":1, "J":9,"Q":10,"K":11, "A":12}

card_size = (81,117)

class Card(object):
    def __init__(self,naipe,value,image,invert_card):
        self.invert_card = invert_card
        self.position = (0,0)
        self.naipe = naipe
        self.value = value
        self.card_image = image

    def set_position(self,x,y):
        self.position = (x,y)
    def draw_card(self,surface):
        surface.blit(self.card_image,self.position)
    def draw_invert_card(self,subsurface):
        surface.blit(self.invert_card,self.position)
    def hover(self,x,y):
        m_x, m_y = self.position
        if x >= m_x and x <= card_size[0]+m_x and y >= m_y and y <= card_size[1] + m_y:
            return True
        return False 
    def __str__(self):
        return "Card({0},{1}-{2}-{3})".format(self.naipe,self.value,self.position[0],self.position[1])

class Deck(object):
    def __init__(self):
        self.position = (0,0)
        self.image = pygame.image.load('images/cards.gif')
        self.deck_image = self.image.subsurface(0*card_size[0],4*card_size[1],
                                                card_size[0],card_size[1])
        self.generate_all_cards()         
        shuffle(self.cards)
    
    def generate_all_cards(self):
        self.cards = list() 
        for naipe in naipes.keys():
            for value in values.keys():
                card_image = self.get_card_image(naipes[naipe],values[value])
                self.cards.append( Card(naipe,value,card_image,self.deck_image))
        
    def shuffle_cards():
        shuffle(self.cards)

    def get_card_image(self,naipe,value):
        frame_rect = pygame.Rect(value*card_size[0],naipe*card_size[1],
                                 card_size[0],card_size[1]) 
        return self.image.subsurface(frame_rect)
    
    def get_card(self):
       return self.cards.pop() 

    def draw(self,surface):
        surface.blit(self.deck_image,self.position)
