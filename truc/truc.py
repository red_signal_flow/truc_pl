from player import Player
from deck import Deck, Card

class Truc(object):
    def __init__(self,surface):
        self.surface = surface
        self.deck = Deck()
        self.mount = list()
        self.players = {}
        self.order = 0
        self.current_player_pick = 0
        self.current_player_play = 0

    def add_players(self,names):
        for name in names:
            self.add_player(name)

    def add_player(self,name):
        if self.order == 0:
            self.players["down"] = Player(name,self.surface,"down")
        if self.order == 1:
            self.players["up"] = Player(name,self.surface,"up")
        if self.order == 2:
            self.players["left"] = Player(name,self.surface,"left")
        if self.order == 3:
            self.players["right"] = Player(name,self.surface,"right")
        self.order = (self.order+1)%4

    def next_player_pick_card(self):
        if self.current_player_pick == 0 and self.players.has_key("down"):
            self.players["down"].pick_card(self.deck.get_card())
        if self.current_player_pick == 1 and self.players.has_key("up"):
            self.players["up"].pick_card(self.deck.get_card())
        if self.current_player_pick == 2 and self.players.has_key("left"):
            self.players["left"].pick_card(self.deck.get_card())
        if self.current_player_pick == 3 and self.players.has_key("right"):
            self.players["right"].pick_card(self.deck.get_card())
        self.current_player_pick= (self.current_player_pick + 1)%len(self.players)

    def players_get_cards(self):
        for x in range(3):
            for player in self.players.values():
                self.next_player_pick_card()

    def draw(self):
        self.deck.draw(self.surface)
        self.draw_mount()
        for player in self.players.values():
            if player == 0:
                continue
            else:
                player.draw()

    def draw_mount(self):
        for card in self.mount:
            card.draw_card(self.surface)
        # if len(self.mount) - 1 >= 0:
        #     self.mount[len(self.mount)-1].draw_card(self.surface)

    def winner(self):
        return True
    def put_in_mount(self,card):
        sw,sh = self.surface.get_size()
        card.set_position( (sw/2) + (len(self.mount) * 20) ,sh/2)
        self.mount.append(card)

    def waiting_next_player_play(self):
        if self.current_player_play == 0 and self.players.has_key("down"):
            card = self.players["down"].play_card()
        if self.current_player_play == 1 and self.players.has_key("right"):
            card = self.players["right"].play_card()
        if self.current_player_play == 2 and self.players.has_key("up"):
            card = self.players["up"].play_card()
        if self.current_player_play == 3 and self.players.has_key("left"):
            card = self.players["left"].play_card()

        if card :
            self.put_in_mount(card)
            self.current_player_play= (self.current_player_play + 1)%len(self.players)

