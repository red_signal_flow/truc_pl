% rules for cards
card(X,Y) :- naipe(X),card_value(Y).
card(Y,X) :- naipe(X),card_value(Y).

% rules for naipes
naipe(ouros).
naipe(copas).
naipe(paus).
naipe(espada).

% rules for values
card_value(2).
card_value(3).
card_value('J').
card_value('Q').
card_value('K').
card_value('A').


% rules for Manilha
manilha(4,paus).
manilha(paus,4).
manilha(7,copas).
manilha(copas,7).
manilha('A',espadas).
manilha(espadas,'A').
manilha(7,ouros).
manilha(ouros,7).

% rules for Numeros
numero(2,X)      :- card(2,X).
numero(X,2)      :- card(X,2).
numero(3,X)      :- card(3,X).
numero(X,3)      :- card(X,3).
numero('A',X)    :- X \= espada , card('A',X).
numero(X,'A')    :- X \= espada , card(X,'A').

% rules for Letras
letra('J',X)     :- card('J',X).
letra(X,'J')     :- card(X,'J').
letra('Q',X)     :- card('Q',X).
letra(X,'Q')     :- card(X,'Q').
letra('K',X)     :- card('K',X).
letra(X,'K')     :- card(X,'K').
