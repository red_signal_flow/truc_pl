from pyswip import Prolog, registerForeign

def test2():
    def hello(t):
        print "Hello,", t
    hello.arity = 1
    registerForeign(hello)
    prolog = Prolog()
    prolog.assertz("father(michael,john)")
    prolog.assertz("father(michael,gina)")    
    list(prolog.query("father(michael,X), hello(X)"))


def test():
    prolog = Prolog()

    prolog.assertz("father(michael,john)")
    prolog.assertz("father(michael,gina)")

    list(prolog.query("father(michael,X)"))

    for soln in prolog.query("father(X,Y)"):
        print soln["X"], "is the father of", soln["Y"]
